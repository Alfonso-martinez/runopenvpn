package com.michael.runopenvpn

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 */
class MainActivity : FragmentActivity() {

    fun setTimeout(delayMillis: Long, callback: () -> Unit) {
        Thread {
            Thread.sleep(delayMillis)
            callback()
        }.start()
    }

    fun commands(cmd: String){
        try {
            val proceso = ProcessBuilder(cmd.split(" "))
                .redirectErrorStream(true)
                .start()
            val reader = BufferedReader(InputStreamReader(proceso.inputStream))
            var linea: String?
            while (reader.readLine().also { linea = it } != null) {
                println(linea)
            }
            val exitCode = proceso.waitFor()
            println("El comando terminó con código de salida: $exitCode")
        } catch (e: Exception) {
            println("El comando fallo")
            e.printStackTrace()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTimeout(2000) {
            commands("am start --user 0 -a android.intent.action.VIEW -n de.blinkt.openvpn/.activities.MainActivity")
            setTimeout(1500) {
                commands("am start --user 0 -a android.intent.action.MAIN -n de.blinkt.openvpn/.api.ConnectVPN --es de.blinkt.openvpn.api.profileName fireTV2")
                commands("am start --user 0 -a android.intent.action.MAIN -n de.blinkt.openvpn/.api.ConnectVPN --es de.blinkt.openvpn.api.profileName fireTV1")
                /*setTimeout(1000) {
                    commands("am start --user 0 -a android.intent.action.MAIN -c android.intent.category.HOME")
                }*/
            }
        }
    }
}